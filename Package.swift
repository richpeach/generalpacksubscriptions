// swift-tools-version: 5.9
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "GeneralPackS",
    platforms: [
        .iOS(.v14)
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "GeneralPackS",
            targets: ["GeneralPackS"]),
    ],
    dependencies: [
        .package(url: "https://github.com/adaptyteam/AdaptySDK-iOS.git", from: "2.9.2")
    ],
    targets: [
        .target(
            name: "GeneralPackS", dependencies: [
                .product(name: "Adapty", package: "AdaptySDK-iOS")
            ])
    ]
)
